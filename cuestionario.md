- 1) que tanto conoces sobre sistemas ERP?
	He colaborado un par de veces en 2 empresas distintas.
- 2) que tanto conoces sobre facturacion electronica?
	Estuve implementando el pago de reserva de citas online para la CRP. Primero usando Culqui.
- 3) que tanto conoces sobre procesos de CI/CD?
	Son estándares de prácticas, que se basan en control de versiones y despliegue.
- 4) que tanto conoces sobre linux?
	Intermedio. Intrascendente a nivel programación. El factor crítico se nota a nivel servidor.
- 5) que te gusta y NO te gusta del lenguaje que usas?
	Me gusta su versatilidad y agilidad. No me gusta que esté plagado de usuarios mediocres.
- 6) si llegas a un punto con algo que no sabes como solucionar que haces?
	Investigo y/o consulto a gente más experta.
- 7) que opinas del trabajo remoto, tienes experiencia?
	Sí, un par de años. Es cómodo y debería ser remoto siempre.
- 8) si te dan una instruccion que no comprendes que haces?
	Solicito otro modo de requerirlo, o lo consulto con otros colegas.
- 9) que opinas de las "horas extras"?
	A veces son necesarias y tienen su normativa establecida en la legislación laboral.
- 10) cuentame cual es la cosa mas dificil que has resuelto.
	Cada cosa nueva que debo aprender de modo autodidacta, es la más difícil en su momento. En cambio, de lo ya aprendido, creo que lo engorroso siempre era cuadrar pdfs para impresión.