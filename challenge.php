<?php

# challenge 1
function getPiDecimal(){
# your code here
	return (M_PI * pow(10, 10)) % 10;
}

# challenge 2
function getSumEvens($a=[1,2,3,4,5,6]){
# your code here
	$r = 0;
	
	foreach($a as $n):
		$r += $n % 2 == 0 ? $n : 0;
	endforeach;
	
	return $r;
}

# challenge 3
function getOrderedVowels($s="just a testing"){
# your code here
	$v = array('a', 'e', 'i', 'o', 'u');
	$r = "";

	foreach(str_split($s) as $l):
		$r .= in_array($l, $v) ? $l : '';
	endforeach;
	
	return $r;
}

# challenge 4
# obtener el primer id del JSON : https://jsonplaceholder.typicode.com/users
function getFirstId(){
# your code here
	$json = file_get_contents('https://jsonplaceholder.typicode.com/users');
	$obj = json_decode($json);
	$item = array_shift($obj);
	
	return $item->id;
}

# DONT EDIT
echo "Running: \n";
echo "challenge 1: ".((getPiDecimal()==5)? "pass" : "fail") . "\n" ;
echo "challenge 2: ".((getSumEvens()==12)? "pass" : "fail" ) . "\n" ;
echo "challenge 3: ".((getOrderedVowels()=="uaei")? "pass" : "fail") . "\n" ;
echo "challenge 4: ".((getFirstId()==1)? "pass" : "fail" ).		 "\n" ;
